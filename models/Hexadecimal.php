<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Hexadecimal extends Model
{
    public $input;

    public function rules()
    {
        return [
            [['input'], 'match', 'pattern' => '/^[0-9A-Fa-f]+$/', 'message' => 'El valor debe ser un número hexadecimal. Solo permite numeros y letars de A a F'],
        ];
    }
}