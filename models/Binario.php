<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Binario extends Model
{
    public $input;

    public function rules()
    {
        return [
            [['input'], 'match', 'pattern' => '/^[01]+$/', 'message' => 'El valor debe ser un número binario. Solo se permiten unos y/o creos'],
        ];
    }
}