<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Decimal extends Model
{
    public $input;

    public function rules()
    {
        return [
            [['input'], 'match', 'pattern' => '/^[0-9]+$/', 'message' => 'El valor debe ser un número. Solo se permiten números enteros.'],
        ];
    }
}