<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Octal extends Model
{
    public $input;

    public function rules()
    {
        return [
            [['input'], 'match', 'pattern' => '/^[0-7]+$/', 'message' => 'El valor debe ser un número octal. Solo se permiten numeros del 1 al 7'],
        ];
    }
}