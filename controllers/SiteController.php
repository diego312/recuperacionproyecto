<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Binario;
use app\models\Decimal;
use app\models\Octal;
use app\models\Hexadecimal;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionBinario()
    {
        $model = new Binario();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $binario = $model->input;
            $decimal = $this->BinDec($binario);
            $octal = $this->BinOct($binario);
            $hexadecimal = $this->BinHex($binario);
            return $this->render('binario', [
                'model' => $model,
                'decimal' => $decimal,
                'octal' => $octal,
                'hexadec' => $hexadecimal,
            ]);
        }

        return $this->render('binario', [
            'model' => $model,
        ]);
    }

    public function actionOctal()
    {
        $model = new Octal();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $octal = $model->input;
            $decimal = $this->OctDec($octal);
            $binario = $this->OctBin($octal);
            $hexadecimal = $this->OctHex($octal);
            return $this->render('octal', [
                'model' => $model,
                'decimal' => $decimal,
                'binario' => $binario,
                'hexadec' => $hexadecimal,
            ]);
        }

        return $this->render('octal', [
            'model' => $model,
        ]);
    }

    public function actionDecimal()
    {
        $model = new Decimal();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $decimal = $model->input;
            $binario = $this->DecBin($decimal);
            $octal = $this->DecOct($decimal);
            $hexadecimal = $this->DecHex($decimal);
            return $this->render('decimal', [
                'model' => $model,
                'binario' => $binario,
                'octal' => $octal,
                'hexadec' => $hexadecimal,
            ]);
        }

        return $this->render('decimal', [
            'model' => $model,
        ]);
    }

    public function actionHexadecimal()
    {
        $model = new Hexadecimal();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $hexadecimal = $model->input;
            $binario = $this->HexBin($hexadecimal);
            $decimal = $this->HexDec($hexadecimal);
            $octal = $this->HexOct($hexadecimal);
            return $this->render('hexadecimal', [
                'model' => $model,
                'decimal' => $decimal,
                'binario' => $binario,
                'octal' => $octal,
            ]);
        }

        return $this->render('hexadecimal', [
            'model' => $model,
        ]);
    }
    
    public function BinDec($inputbin)
    {
        $decimal = 0;
        $base = 1;
        $length = strlen($inputbin);
        
        for ($i = $length - 1; $i >= 0; $i--) {
            if ($inputbin[$i] == '1') {
                $decimal += $base;
            }
            $base = $base * 2;
        }
        
        return $decimal;
    }
    
    public function BinOct($inputbin)
    {
        $long = ceil(strlen($inputbin) / 3) * 3;
        $formceros = str_pad($inputbin, $long, '0', STR_PAD_LEFT);
        $octal = '';
        
        for ($i = 0; $i < strlen($formceros) / 3; $i++) {
            $split = substr($formceros, $i*3, 3);
            $num = 0;
            $base = 1;
            
                for ($j = 2; $j >= 0; $j--) {
                    if ($split[$j] == '1') {
                        $num += $base;
                    }
                    $base = $base * 2;
                }
            $octal .= $num;
        }

        return !empty(ltrim($octal, '0')) ? ltrim($octal, '0') : '0';
    }
    
    public function BinHex($inputbin)
    {
        $long = ceil(strlen($inputbin) / 4) * 4;
        $formceros = str_pad($inputbin, $long, '0', STR_PAD_LEFT);
        $hexadec = '';
        
        for ($i = 0; $i < strlen($formceros) / 4; $i++) {
            $split = substr($formceros, $i*4, 4);
            $decimal = 0;
            $base = 1;
            
                for ($j = 3; $j >= 0; $j--) {
                    if ($split[$j] == '1') {
                        $decimal += $base;
                    }
                    $base = $base * 2;
                }
            switch ($decimal) {
            case 10:
                $decimal = 'A';
                break;
            case 11:
                $decimal = 'B';
                break;
            case 12:
                $decimal = 'C';
                break;
            case 13:
                $decimal = 'D';
                break;
            case 14:
                $decimal = 'E';
                break;
            case 15:
                $decimal = 'F';
                break;
            }
            $hexadec .= $decimal;
        }

        return !empty(ltrim($hexadec, '0')) ? ltrim($hexadec, '0') : '0';
    }
    
    public function DecBin($inputdec)
    {
        $binario = '';
        while ($inputdec > 0) {
            $binario = ($inputdec % 2) . $binario;
            $inputdec = floor($inputdec / 2);
        }
        return ltrim($binario, '0') ? $binario : '0';
    }
    
    public function DecOct($inputdec)
    {
        $octal = '';
        while ($inputdec > 0) {
            $octal = ($inputdec % 8) . $octal;
            $inputdec = floor($inputdec / 8);
        }
        return ltrim($octal, '0') ? $octal : '0';
    }
    
    public function DecHex($inputdec)
    {
        $hexadec = '';
        while ($inputdec > 0) {
            $decimal = $inputdec % 16;
            switch ($decimal) {
            case 10:
                $decimal = 'A';
                break;
            case 11:
                $decimal = 'B';
                break;
            case 12:
                $decimal = 'C';
                break;
            case 13:
                $decimal = 'D';
                break;
            case 14:
                $decimal = 'E';
                break;
            case 15:
                $decimal = 'F';
                break;
            }
            $hexadec = $decimal . $hexadec;
            $inputdec = floor($inputdec / 16);
        }
        return ltrim($hexadec, '0') ? $hexadec : '0';
    }
    
    public function OctBin($inputoct)
    {
        $binario = '';
        $octal = strval($inputoct);
        for ($i = 0; $i < strlen($octal); $i++) {
            $split = intval($octal[$i]);
            $split_binario = '';
            while ($split > 0) {
                $split_binario = ($split % 2) . $split_binario;
                $split = floor($split / 2);
            }
            $split_binario = str_pad($split_binario, 3, '0', STR_PAD_LEFT);
            $binario .= $split_binario;
        }
        return !empty(ltrim($binario, '0')) ? ltrim($binario, '0') : '0';
    }
    
    public function OctDec($inputoct)
    {
        $binario = $this->OctBin($inputoct);
        $decimal = $this->BinDec($binario);
        return $decimal;
    }
    
    public function OctHex($inputoct)
    {
        $binario = $this->OctBin($inputoct);
        $hexadec = $this->BinHex($binario);
        return $hexadec;
    }
    
    public function HexBin($inputhex) 
    {
        $binario = '';
        $inputhex = strtoupper($inputhex);
        for ($i = 0; $i < strlen($inputhex); $i++) {
            $split = substr($inputhex, $i, 1);
            switch ($split) {
                case '0':
                    $binario .= '0000';
                    break;
                case '1':
                    $binario .= '0001';
                    break;
                case '2':
                    $binario .= '0010';
                    break;
                case '3':
                    $binario .= '0011';
                    break;
                case '4':
                    $binario .= '0100';
                    break;
                case '5':
                    $binario .= '0101';
                    break;
                case '6':
                    $binario .= '0110';
                    break;
                case '7':
                    $binario .= '0111';
                    break;
                case '8':
                    $binario .= '1000';
                    break;
                case '9':
                    $binario .= '1001';
                    break;
                case 'A':
                    $binario .= '1010';
                    break;
                case 'B':
                    $binario .= '1011';
                    break;
                case 'C':
                    $binario .= '1100';
                    break;
                case 'D':
                    $binario .= '1101';
                    break;
                case 'E':
                    $binario .= '1110';
                    break;
                case 'F':
                    $binario .= '1111';
                    break;
            }
        }
        return !empty(ltrim($binario, '0')) ? ltrim($binario, '0') : '0';
    }   
    
    public function HexDec($inputoct)
    {
        $binario = $this->HexBin($inputoct);
        $decimal = $this->BinDec($binario);
        return $decimal;
    }
    
    public function HexOct($inputoct)
    {
        $binario = $this->HexBin($inputoct);
        $octal = $this->BinOct($binario);
        return $octal;
    }
}
