<?php

/** @var yii\web\View $this */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Conversor de números octales';
?>

<div class = "my-4">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'input')->textInput(['autocomplete' => "off", 'maxlength' => true, 'pattern' => '[0-7]+', 'required' => true])->label('Introduce el número que deseas convertir') ?>

<div class="form-group">
    <?= Html::submitButton('Convertir', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php if (isset($decimal)): ?>
    <div class="alert alert-success">
        <p>El número binario <?= Html::encode($model->input) ?> es:<br>
            <?= Html::encode($decimal) ?> en decimal<br>
            <?= Html::encode($binario) ?> en binario<br>
            <?= Html::encode($hexadec) ?> en hexadecimal
        </p>
    </div>
<?php endif; ?>