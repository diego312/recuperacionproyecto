<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'ConBitsor';
?>
<?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => '@web/imagenes/favicon.png?v=2']); ?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><strong><?= Html::encode($this->title) ?></strong></h1>

        <p class="lead"><strong>Facil, rapido y simple</strong></p>
        
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-6">
                <p class = "alert alert-success">
                    Bienvenido al conversor de unidades ConBitsor. En esta herramienta, podrás convertir valores de una unidad de medida a otra, de forma sencilla y rápida.<br>
                    Para utilizar el conversor, primero debes seleccionar el tipo de dato que deseas introducir. Puedes elegir entre cuatro opciones: binario, decimal, octal y hexadecimal. Una vez que hayas seleccionado el tipo de dato, escribe el valor numérico que deseas convertir.<br>
                    Al presionar el botón "Convertir", el conversor mostrará el valor que ingresaste en las otras tres unidades de medida que no seleccionaste, es decir, la conversión del valor que ingresaste a binario, decimal, octal y hexadecimal.<br>
                    ¡Utiliza nuestro conversor de unidades y realiza tus conversiones de forma rápida y sencilla!
                </p>
            </div>
            <div class="col-lg-6">
                <?= Html::img('@web/imagenes/Explicacion.jpg', ['alt' => 'Explicacion de uso', 'class' => 'd-block w-100']) ?>
            </div>
        </div>
        <div>
            <p class = "alert alert-success">Selecciona el formato del numero que quieres convertir:</p>
        </div>
        <div class="row">

            <div class="col-sm-3">
                <a href="localhost/Proyecto5/web/index.php?r=site%2Fbinario">
                    <div class="card">
                        <div class="card-footer">
                            <h5 class="card-title">Binario</h5>
                        </div>               
                    </div>
                </a>
            </div>

            <div class="col-sm-3">
                <a href="localhost/Proyecto5/web/index.php?r=site%2Fdecimal">
                    <div class="card">
                        <div class="card-footer">
                            <h5 class="card-title">Decimal</h5>
                        </div>               
                    </div>
                </a>
            </div>

            <div class="col-sm-3">
                <a href="localhost/Proyecto5/web/index.php?r=site%2Foctal">
                    <div class="card">
                        <div class="card-footer">
                            <h5 class="card-title">Octal</h5>
                        </div>               
                    </div>
                </a>
            </div>

            <div class="col-sm-3">
                <a href="localhost/Proyecto5/web/index.php?r=site%2Fhexadecimal">
                    <div class="card">
                        <div class="card-footer">
                            <h5 class="card-title">Hexadecimal</h5>
                        </div>               
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

